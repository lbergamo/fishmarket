import socket

port = 6666
host = '127.0.0.1'
fname = 'fishmarket.py'

s = socket.socket()
s.bind((host, port))
s.listen(5)

print('Server is up!')

while True:
    conn, addr = s.accept()
    print('Connected to {}'.format(':'.join(map(str, list(addr)))))
    
    f = open(fname, 'rb')
    l = f.read(1024)
    while(l):
        conn.send(l)
        l = f.read(1024)
    f.close()
     
    print('Transfer finished.')
    conn.close()
