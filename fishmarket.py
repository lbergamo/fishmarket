import os
import socket
import select
import sys
import pickle
import threading
import time
import tty
import termios

class Comm:

    def __init__(self, mode, port):
        
        self.mode = mode
        self.port = port
        
        if mode == 'server' or mode == 'host':
        
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.bind(('127.0.0.1', port))
            s.listen(5)
            c, addr = s.accept()
            
            self.sock = c
        
        else:
            
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect(('127.0.0.1', port))
            
            self.sock = s
        
    def send(self, msg):
        self.sock.sendall(pickle.dumps(msg))
    
    def recv(self):
        return pickle.loads(self.sock.recv(2*1024))
    
    def close(self):
        self.sock.close()
    
    def shutdown(self):
        self.sock.shutdown(socket.SHUT_RDWR)
    
    
class Channel(threading.Thread):
    def __init__(self, comm):
        threading.Thread.__init__(self)
        self.shutdown_flag = threading.Event()
        self.comm = comm
    
    def run(self):
        while not self.shutdown_flag.is_set():
            try:
                data = self.comm.recv()
                print_msg('[{} {}] {}'.format(data['user'], data['time'], data['msg']))
            except:
                self.shutdown_flag.set()
        self.comm.close()

            
def print_msg(msg):
    global hist
    global write_buf
    hist.append(msg)
    while len(hist)>1000:
        hist.pop(0)
    os.system('clear')
    print(200*'\r\n')
    for line in hist:
        print(line + '\r\n')
    print(''.join(write_buf)),
    sys.stdout.flush()
    
    
def getch():
    ch = sys.stdin.read(1)
    return ch
    
    
def read_single_key():
    x = getch()
    ox = ord(x)
    if ox == 27 or ox == 127:
        sys.stdout.write(chr(8))
        sys.stdout.write(chr(32))
        sys.stdout.write(chr(8))
    elif ox == 3: raise KeyboardInterrupt
    elif ox == 4: raise EOFError
    return x
    
    
parsenum = (lambda num:
        (sys.maxsize if 0 > num else num))

    
def nbsp(x, y):
    if ord(x) == 27 or ord(x) == 127:
        try:
            y.pop()
        except IndexError:
            pass
        return y
    y.append(x)
    return y

    
def until(chars, count=-1):
    y = []
    chars = list(chars)
    count = parsenum(count)
    while len(y) <= count:
        i = read_single_key()
        _ = sys.stdout.write(i)
        sys.stdout.flush()
        if i in chars:
            break
        y = nbsp(i, y)
    return ''.join(y)
    
    
def until_enter(count=-1):
    global hist
    global write_buf
    count = parsenum(count)
    while len(write_buf) <= count:
        i = read_single_key()
        os.system('clear')
        print(200*'\r\n')
        for line in hist:
            print(line + '\r\n')
        if ord(i) == 10 or ord(i) == 13:
            break
        write_buf = nbsp(i, write_buf)
        print(''.join(write_buf)),
    ret = ''.join(write_buf)
    write_buf = []
    return ret
    
    
if __name__ == '__main__':
    
    ports_avail = range(6000, 6020)
    user = os.getlogin()
    hist = []
    write_buf = []
    connected = False
    
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    tty.setraw(sys.stdin.fileno())
    
    for port in ports_avail:
        try:
            comm = Comm('client', port)
            connected = True
            break
        except:
            pass
            
    if connected:
        print('Connected to {}'.format(port))
        data = {}
        data['user'] = user
        data['time'] = '{:02d}:{:02d}'.format(time.localtime()[3], time.localtime()[4])
        data['msg'] = ''
        comm.send(data)
        data = comm.recv()
        print_msg(data['msg'].replace('\n','\r\n'))
        channel = Channel(comm)
        channel.start()
    else:
        print('Could not connect to server.\r\n')
        
    while connected:
        inp = until_enter()
        if inp != '!quit':
            data = {}
            data['user'] = user
            data['time'] = '{:02d}:{:02d}'.format(time.localtime()[3], time.localtime()[4])
            data['msg'] = inp
            comm.send(data)
        else:
            channel.shutdown_flag.set()
            comm.shutdown()
            break
            
    termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    
   
