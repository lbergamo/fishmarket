# FishMarket

FishMarket is simple chat written in python where you can only broadcast messages to everyone.

This chat was developed to run in an environment consisting of one single computer accessed through several terminals, where different users did not have access to common directories and the only possible way of exchanging information was through TCP/IP localhost.

## Usage

* upload_fishmarket.py

	This script uploads fishmarket.py other user. The other user should run download_fishmarket.py to get the file.

* download_fishmarket.py

	This script downloads fishmarket.py. It is a short script that can be typed quickly by any user who wants to get FishMarket.

* fishmarket.py

	The main script for the chat.

* server_fishmarket.py

	This is the FishMarket server. Someone must run this in for the chat to work. All the users connect to the server and the server broadcasts the messages.

## Related software

Red Battleship is another software with the same purpose. A multiplayer battleship game developed to run in the same environment. https://bitbucket.org/lbergamo/rbs/
