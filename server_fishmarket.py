import os
import socket
import sys
import pickle
import threading
import time

class Comm:

    def __init__(self, mode, port):
        
        self.mode = mode
        self.port = port
        
        if mode == 'server' or mode == 'host':
        
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.bind(('127.0.0.1', port))
            s.listen(5)
            c, addr = s.accept()
            
            self.sock = c
        
        else:
            
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect(('127.0.0.1', port))
            
            self.sock = s
        
    def send(self, msg):
        self.sock.sendall(pickle.dumps(msg))
    
    def recv(self):
        return pickle.loads(self.sock.recv(2*1024))
    
    def close(self):
        self.sock.close()
    
    def shutdown(self):
        self.sock.shutdown(socket.SHUT_RDWR)
    
    
def broadcast(data, clients):
    for client in clients:
        try:
            clients[client].send(data)
        except:
            pass
            
            

class Channel(threading.Thread):
    def __init__(self, port):
        threading.Thread.__init__(self)
        self.port = port
        self.shutdown_flag = threading.Event()
        self.user = None
    
    def run(self):
        global clients
        global channels
#        print('{} port started'.format(self.port))

        if not self.shutdown_flag.is_set():
            try:
                clients[self.port] = Comm('server', port)
            except:
                self.shutdown_flag.set()
        
        if not self.shutdown_flag.is_set():
#        print('Connected to {}'.format(self.port))
            try:
                data = clients[self.port].recv()
                self.user = data['user']
            except:
                print('Failed to receive user name.')
                self.shutdown_flag.set()
            try:
                data = {}
                data['user'] = 'FishMarket'
                data['msg'] = splash()
                clients[self.port].send(data)
            except:
                print('Failed to send welcome message.')
                self.shutdown_flag.set()
            
        while not self.shutdown_flag.is_set():
            try:
                data = clients[self.port].recv()
                data['user'] = user_dict.get(data['user'], data['user'])
                if data['msg'] == '!online':
                    data['user'] = 'FishMarket'
                    msg = '#### Online users ####\r\n'
                    for client in clients:
                        user_name = user_dict.get(channels[client].user, channels[client].user)
                        msg = msg + '                   ' + user_name + '\r\n'
                    msg = msg[:-2]
                    data['msg'] = msg
                    clients[self.port].send(data)
                else:
                    broadcast(data, clients)
            except:
                self.shutdown_flag.set()
                try:
                    #clients[self.port].close()
                    clients[self.port].shutdown()
                except:
                    pass
#                    print('Error closing')

#        print('{} has disconnected'.format(self.port))
#                self.run()
        try:
            clients.pop(self.port)
        except:
            pass

            

            
            
def splash():
    s = []
    s.append(r'                    ___                                      ' + '\n')
    s.append(r'     ___======____=---=)                                     ' + '\n')
    s.append(r'  /T             \_--===)                                    ' + '\n')
    s.append(r'  [ \ (0)   \~     \_-==)                                    ' + '\n')
    s.append(r'   \      / )J~~     \-=)                                    ' + '\n')
    s.append(r'    \\___/  )JJ ~~~   \)                                     ' + '\n')
    s.append(r'     \_____/JJJ~~~~     \                                    ' + '\n')
    s.append(r'     / \     \J~~~~~     \                                   ' + '\n')
    s.append(r'    (-\)\=|\\\~~~~        L__                                ' + '\n')
    s.append(r'    (\\)  (\\\)_            \==__                            ' + '\n')
    s.append(r'     \V    \\\) ===______   \\\\\\                           ' + '\n')
    s.append(r'            \V      \_)  \\\\JJ\J\)                          ' + '\n')
    s.append(r'                         /J\JT\JJJJ)   Welcome to FishMarket!' + '\n')
    s.append(r'                         (JJJ| \UUU)   It feels like home... ' + '\n')
    s.append(r'                          (UU)         Type !quit to quit.   ' + '\n')
    s.append(r'                                                             ' + '\n')
    return ''.join(s)
    
if __name__ == '__main__':
    
    clients = {}
    channels = {}
    ports_avail = range(6000, 6020)
    status = {}
    user_dict = {'user1':'Name 1',
                 'user2':'Name 2'}
    
    for port in ports_avail:
        channels[port] = Channel(port)
        channels[port].start()
    
    t0 = time.time()
    online = []
    online_old = []
    last_users = {}
    while True:
        try:
            for port in ports_avail:
                if channels[port].is_alive():
                    if clients.has_key(port):
                        status[port] = 'BUSY'
                    else:
                        status[port] = 'UP'
                
                else:
                    status[port] = '--'
                    channels[port] = Channel(port)
                    channels[port].start()
                    
            online = clients.keys()
            for port in list(set(online) - set(online_old)):
                last_users[port] = channels[port].user
            
            new_users = {}
            old_users = {}
            for port in list(set(online_old) - set(online)):
                old_users[port] = last_users[port]
            
            for port in list(set(online) - set(online_old)):
                new_users[port] = last_users[port]
            
            for user in new_users:
                data = {}
                data['user'] = 'FishMarket'
                user_name = user_dict.get(new_users[user], new_users[user])
                data['msg'] = '#### {} has joined the market ####'.format(user_name)
                data['time'] = '{:02d}:{:02d}'.format(time.localtime()[3], time.localtime()[4])
                broadcast(data, clients)
            
            for user in old_users:
                data = {}
                data['user'] = 'FishMarket'
                user_name = user_dict.get(old_users[user], old_users[user])
                data['msg'] = '#### {} has left the market ####'.format(user_name)
                data['time'] = '{:02d}:{:02d}'.format(time.localtime()[3], time.localtime()[4])
                broadcast(data, clients)
                
            online_old = online
            
            os.system('clear')
            for port in ports_avail:
                user_name = user_dict.get(channels[port].user, channels[port].user)
                print('[{}] {} {}'.format(port, status[port], user_name))
            t0 = time.time()
            
            time.sleep(0.5)
            
        except KeyboardInterrupt:
            break
    
    os.system('clear')
    print('Stopping server...')
    for port in ports_avail:
        while channels[port].is_alive():
            channels[port].shutdown_flag.set()
            try:
                clients[port].shutdown()
            except:
                try:
                    comm = Comm('client', port)
                    comm.shutdown()
                except:
                    pass
    
    for port in channels:
        if not channels[port].is_alive():
            status[port] = 'DOWN'
    
    os.system('clear')
    for port in ports_avail:
        user_name = user_dict.get(channels[port].user, channels[port].user)
        print('[{}] {} {}'.format(port, status[port], user_name))
    
    print('')
    print('Server is down!')
                
                
                
                
                
                
                
                
                
